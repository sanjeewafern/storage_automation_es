#include <Stepper.h>
#include <Servo.h>

//FUNCTION DEFINITIONS
void printarray1(boolean array1[2][2]);
void readData(String userRowInput, String userColInput);
void checkLoadOrUnload(int userRowInput, int userColInput, boolean array1[3][2]);
void motorOneControlClockwise();
void motorOneControlAnticlockwise();
void motorTwoControlClockwise();
void motorTwoControlAnticlockwise();
void motorThreeControlClockwise(void);
void motorThreeControlAnticlockwise(void);

// Motor 1 variables (place 1)
const int stepPin = 9;
const int dirPin = 8;
const int enPin = 13;

// Motor 2 variables (place 1)
#define STEPS 2038
//int stepCount = 0;
Stepper stepper(STEPS, A0, A2, A1, A3);

// Motor 3 variables (place 1)
Servo M3;
int pos = 0;

String userRowInput = "";
String userColInput = "";

const int dRows = 2;
const int dCols = 2;

//To keep track of the status of each Rack (empty / full)
boolean array1[dRows][dCols] = {{0, 0}, {0, 0}};

void setup() {                                                //setup

  Serial.begin(9600);

  // Motor 1 (place 2)
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(enPin, OUTPUT);
  digitalWrite(enPin, LOW);

  // Motor 3 (place 2)
  M3.attach(4);

  //printarray1(array1);
  Serial.println("***   Program Begins   ***");

  //readData(userRowInput, userColInput);
  checkLoadOrUnload(0, 0, array1);

  //stepper.setSpeed(4);
  //stepper.step(200);

  //motorThreeControlClockwise();
  //motorThreeControlAnticlockwise();

  //  motorOneControlClockwise();
  //  motorOneControlClockwise();
  //  motorOneControlAnticlockwise();
  //motorOneControlAnticlockwise();

  //motorTwoControlAnticlockwise();//goin up
  //  motorTwoControlClockwise();//goin down

  //motorThreeControlClockwise();
  //motorThreeControlAnticlockwise();
}

void loop() {
}

void motorOneControlAnticlockwise() {
  digitalWrite(dirPin, HIGH);    //  anticlockwise direction

  for (int x = 0; x < 800; x++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000);
  }
  delay(1000); // One second delay
}

void motorOneControlClockwise() {
  digitalWrite(dirPin, LOW);   // clockwise driection

  for (int x = 0; x < 800; x++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000);
  }
  delay(1000);
}

void motorTwoControlClockwise() {
  // 1 rpm
  stepper.setSpeed(4);
  // do 2038 steps -- corresponds to one revolution in one minute
  stepper.step(2038);
  // wait for one second
  delay(1000);
}

void motorTwoControlAnticlockwise() {
  stepper.setSpeed(4);
  // do 2038 steps in the other direction with faster speed -- corresponds to one revolution in 10 seconds
  stepper.step(-2038);
  delay(1000);
}

//Motor 2 Little movement Clockwise 500 steps **************
void motorTwoControlLittleClockwise() {
  stepper.setSpeed(4);
  stepper.step(700);
  delay(1000);
}

//Motor 2 Little movement Anticlockwise 500 steps **********
void motorTwoControlLittleAnticlockwise() {
  stepper.setSpeed(4);
  stepper.step(-700);
  delay(1000);
}

//Motor 2 Little movement Clockwise 600 steps***************
void motorTwoControlLittleClockwise600() {
  stepper.setSpeed(4);
  stepper.step(600);
  delay(1000);
}

//Motor 2 Little movement Anticlockwise 600 steps **********
void motorTwoControlLittleAnticlockwise600() {
  stepper.setSpeed(2);
  stepper.step(-600);
  delay(1000);
}

void motorThreeControlClockwise(void) {
  for (pos = 0; pos <= 180; pos += 8) {
    // in steps of 1 degree
    M3.write(pos);
    delay(200);
  }
}

void motorThreeControlAnticlockwise(void) {
  for (pos = 180; pos >= 0; pos -= 8) {
    M3.write(pos);
    delay(200);
  }
}

void readData(String userRowInput, String userColInput) {
  Serial.println("Enter the row number : ");
  while (Serial.available() == 0) {
    //Wait for user input
  }
  userRowInput = Serial.readString(); //Reading the Input string from Serial port.

  Serial.println("Enter the col number : ");
  while (Serial.available() == 0) {
    //Wait for user input
  }
  userColInput = Serial.readString();

  int intVarRow = userRowInput.toInt();
  int intVarCol = userColInput.toInt();

  Serial.println("-------------------------");  //Showing the details
  Serial.println("ROW NO IS : " + userRowInput);
  Serial.println("COL NO IS : " + userColInput);

  Serial.println(intVarRow);
  Serial.println(intVarCol);

  Serial.println("Thank You...");
  Serial.println("");
  //  while (Serial.available() == 0) {
  //  }
}

void checkLoadOrUnload(int userRowInput, int userColInput, boolean array1[2][2]) {
  //check with the array1 stored values
  if (array1[userRowInput][userColInput]) {

    //Item exists on the rack. Ready for an unload
    Serial.println(array1[userRowInput][userColInput]);
    Serial.println("Ready for an unload");

    // row*oneRev column*oneRev;

    for (int i = 0; i <= userColInput; i++) {
      //run the program Motor 1
      motorOneControlClockwise();
    }

    for (int i = 0; i <= userRowInput; i++) {
      //run the program Motor 2
      motorTwoControlClockwise();
    }

    motorThreeControlAnticlockwise();

    //another up movement required here...

    //little movement up
    motorTwoControlLittleClockwise();

    motorThreeControlClockwise();

    //little movement down (resetting the position)
    motorTwoControlLittleAnticlockwise();

    motorTwoControlAnticlockwise();

    for (int i = 0; i <= userColInput; i++) {
      //run the program Motor 1
      motorOneControlAnticlockwise();
    }

  }

  else {
    Serial.println("Please place the Item on the holder and press 99 when u are ready");
    int loadNow = 0;
    int matchInput = 99;

    while (true) {
      if (Serial.available() > 0) {
        //Wait for user input
        loadNow = Serial.parseInt();
        //Serial.println(loadNow);
        //Serial.println(matchInput);
        break;
      }
    }

    if (loadNow == matchInput) {
      Serial.println("Got it!");

      for (int i = 0; i <= userColInput; i++) {
        //run the program Motor 1
        motorOneControlClockwise();
        Serial.println("Motor 1 executed");
      }

      for (int i = 0; i <= userRowInput; i++) {
        //run the program Motor 2
        motorTwoControlClockwise();
        Serial.println("Motor 2 executed");
      }

      //600 code here
      motorTwoControlLittleClockwise600();

      motorThreeControlAnticlockwise();
      Serial.println("Everything done baby");

      motorTwoControlLittleAnticlockwise600();

      motorThreeControlClockwise();
      Serial.println("Motor 3 executed");

      for (int i = 0; i <= userRowInput; i++) {
        //run the program Motor 2
        motorTwoControlAnticlockwise();
        Serial.println("Motor 2 executed");
      }

      for (int i = 0; i <= userColInput; i++) {
        //run the program Motor 1
        motorOneControlAnticlockwise();
        Serial.println("Motor 1 executed");
      }


    } else {
      Serial.println("Invalid Input");
    }
  }
}

void printarray1(boolean array1[2][2]) {
  for ( int i = 0; i < dRows; ++i ) {
    for ( int j = 0; j < dCols; ++j )
      Serial.print (array1[ i ][ j ] );
    Serial.println("");
  }
}
